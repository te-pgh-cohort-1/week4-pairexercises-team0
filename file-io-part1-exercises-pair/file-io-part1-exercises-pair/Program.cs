﻿using System;
using System.IO;
using System.Collections.Generic;

namespace file_io_part1_exercises_pair {
    class Program {
        static void Main(string[] args) {
            Console.Write("Please input filename that is to be read: ");
            //@"alices_adventures_in_wonderland.txt"
            string fileName = Console.ReadLine();
            //@"C:\Users\Kyle S\workspace\exercises\week4-pairexercises-team0\file-io-part1-exercises-pair"
            Console.Write("Please input the path to the folder the contains the file: ");
            string path = Console.ReadLine();
            Console.WriteLine();
            string filePath = Path.Combine(path, fileName);
            List<string> words = new List<string>();
            List<string> sentences = new List<string>();
            try {
                using (StreamReader alice = new StreamReader(filePath)) {
                    while (!alice.EndOfStream) {
                        string input = alice.ReadLine();

                        string[] inputArray = input.Split(" ", StringSplitOptions.RemoveEmptyEntries);

                        foreach (string ins in inputArray) {
                            words.Add(ins);
                        }
                    }
                }
                using (StreamReader alice = new StreamReader(filePath)) {
                    while (!alice.EndOfStream) {
                        string inputEnd = alice.ReadToEnd();

                        string[] exclamationArray = inputEnd.Split("!", StringSplitOptions.RemoveEmptyEntries);
                        string[] periodArray = inputEnd.Split(".", StringSplitOptions.RemoveEmptyEntries);
                        string[] questionArray = inputEnd.Split("?", StringSplitOptions.RemoveEmptyEntries);

                        foreach (string ex in exclamationArray) {
                            sentences.Add(ex);
                        }
                        foreach (string period in periodArray) {
                            sentences.Add(period);
                        }
                        foreach (string question in questionArray) {
                            sentences.Add(question);


                        }
                    }
                }
            }
            catch (FileNotFoundException) {
                Console.WriteLine("The file wasn't found.");
            }
            catch (IOException) {
                Console.WriteLine("Error reading the file.");
            }
            Console.WriteLine("There are " + words.Count + " words in this file.");
            Console.WriteLine($"There are {sentences.Count} sentences in this file.");
            Console.ReadLine();
        }
    }
}
