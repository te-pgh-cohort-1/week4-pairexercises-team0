﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FindAndReplace {
    class Program {
        static void Main(string[] args) {
            string fileName = "";
            string path = "";
            string sourceFilePath = "";

            do {
                Console.Write("Please input source filename that is to be read: ");
                //" @"alices_adventures_in_wonderland.txt";
                fileName = Console.ReadLine();
                //@"C:\Users\Kyle S\workspace\exercises\week4-pairexercises-team0\file-io-part2-exercises-pair"
                Console.Write("Please input the path to the folder the contains the file: ");
                path = Console.ReadLine();
                //@"C:\Users\Kyle S\workspace\exercises\week4-pairexercises-team0\file-io-part2-exercises-pair"
                sourceFilePath = Path.Combine(path, fileName);
            } while (!File.Exists(sourceFilePath));

            Console.Write("Please input the search phrase: ");
            string searchPhrase = Console.ReadLine();
            searchPhrase = $" {searchPhrase} ";

            Console.Write("Please input the replace phrase: ");
            string replacephrase = Console.ReadLine();
            replacephrase = $" {replacephrase} ";

            Console.WriteLine();
            

            Console.Write("Please input destination filename that is to be read: ");
            //@"alices_adventures_in_wonderland5.txt";
            string destFileName = Console.ReadLine();

            Console.Write("Please input the path to the folder the contains the file: ");
            string destPath = Console.ReadLine();
            //@"C:\Users\Kyle S\workspace\exercises\week4-pairexercises-team0\file-io-part2-exercises-pair"

            string destFilePath = Path.Combine(destPath, destFileName);
            List<string> words = new List<string>();

            //if (File.Exists(destFilePath) || Directory.Exists(destPath)) {
            //    Console.WriteLine("The file or directory already exist");
            //    throw new Exception();
            //}

            try {
                using (StreamReader alice = new StreamReader(sourceFilePath)) {
                    using (StreamWriter output = new StreamWriter(destFilePath, false))
                        while (!alice.EndOfStream) {
                            string input = alice.ReadLine();
                            string inputArray = input.Replace(searchPhrase, replacephrase);
                            output.WriteLine(inputArray);
                        }
                }
            }
            catch (FileNotFoundException) {
                Console.WriteLine("The file wasn't found.");
            }
            
        }
    }
}

