using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercises.Classes;
namespace Exercises.Test
{
    [TestClass]
    public class StringCalculatorTest
    {
        StringCalculator Barry = new StringCalculator();
        [TestMethod]
        public void AddTests()
        {
            string empty = "";
            string single = "1";
            string doubles = "1,2";


            Assert.AreEqual(0, Barry.Add(empty));
            Assert.AreEqual(1, Barry.Add(single));
            Assert.AreEqual(3, Barry.Add(doubles));
        }
        [TestMethod]
        public void AddNewLineTests() {
            string empty = "";
            string single = "1";
            string doubles = "1\n2";


            Assert.AreEqual(0, Barry.Add(empty));
            Assert.AreEqual(1, Barry.Add(single));
            Assert.AreEqual(3, Barry.Add(doubles));
        }
    }
}
