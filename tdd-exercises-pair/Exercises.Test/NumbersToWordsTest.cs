﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Exercises.Classes;

namespace Exercises.Test {
    [TestClass]
    public class NumbersToWordsTest {
        NumberToWords Numbers = new NumberToWords();
        [TestMethod]
        public void ZeroAndTwenty() {
            string empty = "19";
            string single = "Nineteen";

            Assert.AreEqual(single, Numbers.NumsToWords(empty));
        }
        [TestMethod]
        public void Twentyto99() {
            string empty = "27";
            string single = "Twenty Seven";
            string test2 = "15";
            string result2 = "Fifteen";

            Assert.AreEqual(single, Numbers.NumsToWords(empty));
            Assert.AreEqual(result2, Numbers.NumsToWords(test2));
        }
        [TestMethod]
        public void Hundreds() {
            string empty = "277";
            string single = "Two Hundred Seventy Seven";
            string test2 = "215";
            string result2 = "Two Hundred Fifteen";

            Assert.AreEqual(single, Numbers.NumsToWords(empty));
            Assert.AreEqual(result2, Numbers.NumsToWords(test2));
        }
        [TestMethod]
        public void Thousands() {
            string empty = "2777";
            string single = "Two Thousand Seven Hundred Seventy Seven";
            string test2 = "2715";
            string result2 = "Two Thousand Seven Hundred Fifteen";

            Assert.AreEqual(single, Numbers.NumsToWords(empty));
            Assert.AreEqual(result2, Numbers.NumsToWords(test2));
        }
        [TestMethod]
        public void TenThousands() {
            string empty = "27777";
            string single = "Twenty Seven Thousand Seven Hundred Seventy Seven";
            string test2 = "27715";
            string result2 = "Twenty Seven Thousand Seven Hundred Fifteen";

            Assert.AreEqual(single, Numbers.NumsToWords(empty));
            Assert.AreEqual(result2, Numbers.NumsToWords(test2));
        }
        [TestMethod]
        public void HundredThousands() {
            string empty = "277777";
            string single = "Two Hundred Seventy Seven Thousand Seven Hundred Seventy Seven";
            string test2 = "277715";
            string result2 = "Two Hundred Seventy Seven Thousand Seven Hundred Fifteen";

            Assert.AreEqual(single, Numbers.NumsToWords(empty));
            Assert.AreEqual(result2, Numbers.NumsToWords(test2));
        }
        [TestMethod]
        public void Zero() {
            string empty = "0";
            string single = "Zero";

            Assert.AreEqual(single, Numbers.NumsToWords(empty));
        }
    }
}
