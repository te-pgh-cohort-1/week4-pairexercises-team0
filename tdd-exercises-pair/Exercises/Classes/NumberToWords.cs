﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercises.Classes {
    public class NumberToWords {

        public string NumsToWords(string numbers) {
            int length = numbers.Length;
            string[] values = new string[numbers.Length];
            for(int i =0; i < numbers.Length; i++) {
                values[i] = numbers.Substring(i,1);
            }
            int intnumbers = int.Parse(numbers);
            int intLastTwo = 0;
            if(numbers.Length >1) {
                intLastTwo = int.Parse(numbers.Substring(numbers.Length - 2, 2));
            }
            string result = "";
            int intConvert = 0;
            int intConvert2 = 0;
            int intConvert3 = 0;
            int intConvert4 = 0;
            int intConvert5 = 0;
            int intConvert6 = 0;

            Dictionary<double, string> ZeroTo19Dict = new Dictionary<double, string>();
            ZeroTo19Dict.Add(0, "");
            ZeroTo19Dict.Add(1, "One");
            ZeroTo19Dict.Add(2, "Two");
            ZeroTo19Dict.Add(3, "Three");
            ZeroTo19Dict.Add(4, "Four");
            ZeroTo19Dict.Add(5, "Five");
            ZeroTo19Dict.Add(6, "Six");
            ZeroTo19Dict.Add(7, "Seven");
            ZeroTo19Dict.Add(8, "Eight");
            ZeroTo19Dict.Add(9, "Nine");
            ZeroTo19Dict.Add(10, "Ten");
            ZeroTo19Dict.Add(11, "Eleven");
            ZeroTo19Dict.Add(12, "Twelve");
            ZeroTo19Dict.Add(13, "Thirteen");
            ZeroTo19Dict.Add(14, "Fourteen");
            ZeroTo19Dict.Add(15, "Fifteen");
            ZeroTo19Dict.Add(16, "Sixteen");
            ZeroTo19Dict.Add(17, "Seventeen");
            ZeroTo19Dict.Add(18, "Eighteen");
            ZeroTo19Dict.Add(19, "Nineteen");
            ZeroTo19Dict.Add(20, "Twenty");

            Dictionary<double, string> TensDict = new Dictionary<double, string>();
            TensDict.Add(2, "Twenty");
            TensDict.Add(3, "Thirty");
            TensDict.Add(4, "Forty");
            TensDict.Add(5, "Fifty");
            TensDict.Add(6, "Sixty");
            TensDict.Add(7, "Seventy");
            TensDict.Add(8, "Eighty");
            TensDict.Add(9, "Ninety");
            if(intnumbers == 0) {
                return "Zero";
            }
            if (intnumbers < 21) {
                result = ZeroTo19Dict[intnumbers];
            }
            else if (length == 2) {
                intConvert = int.Parse(values[0]);
                intConvert2 = int.Parse(values[1]);
                result = TensDict[intConvert] + " " + ZeroTo19Dict[intConvert2];
            }
            else if (length == 3) {
                intConvert = int.Parse(values[0]);
                intConvert2 = int.Parse(values[1]);
                intConvert3 = int.Parse(values[2]);
                if (intLastTwo < 21) {
                    result = ZeroTo19Dict[intConvert] + " Hundred " + ZeroTo19Dict[intLastTwo];

                }
                else {
                    result = ZeroTo19Dict[intConvert] + " Hundred " + TensDict[intConvert2] + " " + ZeroTo19Dict[intConvert3];
                }
            }
            else if (length == 4) {
                intConvert = int.Parse(values[0]);
                intConvert2 = int.Parse(values[1]);
                intConvert3 = int.Parse(values[2]);
                intConvert4 = int.Parse(values[3]);
                if (intLastTwo < 21) {
                    result = ZeroTo19Dict[intConvert] + " Thousand " + ZeroTo19Dict[intConvert2] + " Hundred "+ ZeroTo19Dict[intLastTwo];

                }
                else {
                    result = ZeroTo19Dict[intConvert] + " Thousand " + ZeroTo19Dict[intConvert2] + " Hundred " + TensDict[intConvert3] + " " + ZeroTo19Dict[intConvert4];
                }
            }
            else if (length == 5) {
                intConvert = int.Parse(values[0]);
                intConvert2 = int.Parse(values[1]);
                intConvert3 = int.Parse(values[2]);
                intConvert4 = int.Parse(values[3]);
                intConvert5 = int.Parse(values[4]);
                if (intLastTwo < 21) {
                    result = TensDict[intConvert] + " " + ZeroTo19Dict[intConvert2] + " Thousand " + ZeroTo19Dict[intConvert3] + " Hundred " + ZeroTo19Dict[intLastTwo];
                }
                else {
                    result = TensDict[intConvert] +" " + ZeroTo19Dict[intConvert2] + " Thousand "  + ZeroTo19Dict[intConvert3] + " Hundred "  + TensDict[intConvert4] + " " + ZeroTo19Dict[intConvert5];
                }
            }
            else if (length == 6) {
                intConvert = int.Parse(values[0]);
                intConvert2 = int.Parse(values[1]);
                intConvert3 = int.Parse(values[2]);
                intConvert4 = int.Parse(values[3]);
                intConvert5 = int.Parse(values[4]);
                intConvert6 = int.Parse(values[5]);
                if (intLastTwo < 21) {
                    result = ZeroTo19Dict[intConvert] + " Hundred " + TensDict[intConvert2] + " " + ZeroTo19Dict[intConvert3] + " Thousand " + ZeroTo19Dict[intConvert4] + " Hundred " + ZeroTo19Dict[intLastTwo];
                }
                else {
                    result = ZeroTo19Dict[intConvert] + " Hundred " + TensDict[intConvert2] + " " + ZeroTo19Dict[intConvert3] + " Thousand " + ZeroTo19Dict[intConvert4] + " Hundred " + TensDict[intConvert5] + " " + ZeroTo19Dict[intConvert6];
                }
            }
            else {

            }

            return result;
        }
    }
}